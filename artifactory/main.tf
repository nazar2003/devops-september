provider "aws" {
  region = "ap-southeast-1"
  access_key=""
  secret_key=""


}
resource "aws_instance" "artifactory" {
  ami = "ami-6f198a0c"
  instance_type = "t2.medium"
  security_groups = ["devops"]
  tags {
    "Name" = "artifactory"
  }
  key_name = "siamol"
  user_data = "${file("installArtifactory")}"


}